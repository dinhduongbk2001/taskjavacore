package fpt.code.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import fpt.code.entities.Department;
import fpt.code.entities.Inoffice;
import fpt.code.entities.Result;
import fpt.code.entities.Student;
import fpt.code.service.ManagentStudent;
import fpt.code.utils.Validator;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ManagentStudent managentStudent = new ManagentStudent();
		while (true) {
			System.out.println("====== Manage Program======");
			System.out.println("1. add student: ");
			System.out.println("2. get by score of student by semester: ");
			System.out.println("3. Check isStudent: ");
			System.out.println("4. add department: ");
			System.out.println("5. Get Sum Student Official : ");
			System.out.println("6. Get Student Has entrypoint Max in each Department: ");
			System.out.println("7. Get Student Inoffice has address: ");
			System.out.println("8. Get List Student medium_core > 8 each Department: ");
			System.out.println("9. Get List Student medium_core max each Department: ");
			System.out.println("10. Get count Student by year each Department: ");
			System.out.println("11.Sorted by comparator   to year and entrypoint: ");
			System.out.println("12.Sorted by comparable   to year and entrypoint: ");
			System.out.println("13. close program Do it : ");
			String line = scanner.nextLine();
			switch (line) {
			case "1": {
				if (managentStudent.getDepartments().size() == 0) {
					System.out.println("Hay add derpatment truoc khi them hoc sinh !!!!!!");
					break;
				} else {
					System.out.println("Enter a: to add Student Official");
					System.out.println("Enter b: to add student Inoffice ");
					String type = scanner.nextLine();
					switch (type) {
					case "a": {
						try {
							System.out.println("You will add student official: ");
							int i = 0;
							List<String> arr = new ArrayList<>();
							for (String key : managentStudent.getDepartments().keySet()) {
								System.out.println("Enter " + i + " to add Student in " + key);
								arr.add(key);
								i++;
							}
							System.out.print("Please choose number Department: ");
							int number1 = scanner.nextInt();
							while (number1 < 0 || number1 >= arr.size()) {
								System.out.println("Enter choose number Department: ");
								number1 = scanner.nextInt();
							}
							String name_department = arr.get(number1);
							scanner.nextLine();
							List<Result> list = new ArrayList<>();
							System.out.print("Enter data id: ");
							String id = Validator.inputTypeString();
//							String id = String.valueOf(scanner.nextLine());
							System.out.print("Enter data name: ");
							String name = Validator.inputTypeString();
//							String name = String.valueOf(scanner.nextLine());
							while (!Validator.isFullname(name)) {
								System.out.println("Enter name again");
								name = scanner.nextLine();
							}
							System.out.print("Enter data birthday: ");
							String birthday = scanner.nextLine();
							while (!Validator.isBirthDay(birthday)) {
								System.out.println("Enter date again");
								birthday = scanner.nextLine();
							}
							System.out.print("Enter data year: ");
							int year = Validator.inputTypeInt();
//							int year = scanner.nextInt();
							scanner.nextLine();
							System.out.print("Enter data entry_point: ");
							Double entry_point = Validator.inputTypeDouble();
//							Double entry_point = scanner.nextDouble();
							scanner.nextLine();
							System.out.print("Enter number of semester: ");
							int number = Validator.inputTypeInt();
//							int number = scanner.nextInt();
							scanner.nextLine();
							list = managentStudent.foreachResults(number);
							Student sto = new Student(id, name, birthday, year, entry_point, list);
							managentStudent.addStudent(sto);
							managentStudent.getDepartments().get(name_department).add(sto);
							System.out.println(sto.toString());
						} catch (Exception ex) {
							// TODO: handle exception
							System.out.println(ex);
						}
						break;

					}
					case "b": {
						try {
							System.out.println("You will add student Inoffice: ");

							int i = 0;
							List<String> arr = new ArrayList<>();
							for (String key : managentStudent.getDepartments().keySet()) {
								System.out.println("Enter " + i + " to add Student in " + key);
								arr.add(key);
								i++;
							}
							System.out.print("Please choose number Department: ");
							int number1 = scanner.nextInt();
							while (number1 < 0 || number1 >= arr.size()) {
								System.out.println("Enter choose number Department: ");
								number1 = scanner.nextInt();
							}
							String name_department = arr.get(number1);
							scanner.nextLine();
							List<Result> list = new ArrayList<>();
							System.out.print("Enter data id: ");
							String id = Validator.inputTypeString();
//							String id = String.valueOf(scanner.nextLine());
							System.out.print("Enter data name: ");
							String name = Validator.inputTypeString();
//							String name = String.valueOf(scanner.nextLine());
							while (!Validator.isFullname(name)) {
								System.out.println("Enter name again");
								name = scanner.nextLine();
							}
							System.out.print("Enter data birthday: ");
							String birthday = scanner.nextLine();
							while (!Validator.isBirthDay(birthday)) {
								System.out.println("Enter date again");
								birthday = scanner.nextLine();
							}
							System.out.print("Enter data year: ");
							int year = Validator.inputTypeInt();
//							int year = scanner.nextInt();
							scanner.nextLine();
							System.out.print("Enter data entry_point: ");
							Double entry_point = Validator.inputTypeDouble();
//							Double entry_point = scanner.nextDouble();
							scanner.nextLine();
							System.out.print("Enter number of semester: ");
							int number = Validator.inputTypeInt();
//							int number = scanner.nextInt();
							scanner.nextLine();
							System.out.print("Enter address: ");
							String address = Validator.inputTypeString();
//							String address = scanner.nextLine();
							list = managentStudent.foreachResults(number);
							Student sti = new Inoffice(id, name, birthday, year, entry_point, list, address);
							managentStudent.addStudent(sti);
							managentStudent.getDepartments().get(name_department).add(sti);
							System.out.println(sti.toString());
						} catch (Exception ex) {
							// TODO: handle exception
							System.out.println(ex);
						}
						break;
					}

					default:
						System.out.println("Try it !!!");
						break;
					}
				}

				break;

			}
			case "2": {

				try {
					System.out.print("Enter id  of student: ");
					String id = Validator.inputTypeString();
//					String id = scanner.nextLine();
					System.out.print("Enter semester  that you want to find: ");
					int semeter = Validator.inputTypeInt();
//					int semeter = scanner.nextInt();
					managentStudent.getMediumScoreOfStudent(id, semeter).forEach(obj -> {
						System.out.println(obj.toString());
					});
				} catch (Exception ex) {
					System.out.println(ex);
				}
				break;
			}
			case "3": {
				try {
					System.out.print("Enter id  of student: ");
					String id = Validator.inputTypeString();
//					String id = scanner.nextLine();
					managentStudent.isOfficial(id);
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
				}
				break;
			}
			case "4": {
				try {
					System.out.print("Enter name department: ");
					String name_dep = Validator.inputTypeString();
//					String name_dep = scanner.nextLine();
					List<Student> listStudents = new ArrayList<>();
					System.out.print("Please choose number Student of Department: ");
					int n = scanner.nextInt();
					while (n <= 0) {
						System.out.println("Enter choose number Department: ");
						n = scanner.nextInt();
					}
					System.out.println("You will add " + n + " student official in Department: ");
					//
					System.out.print(
							"Please choose TYPE Student of Department : 0 with Student Official and 1 with Student Inoffice ");
					int line3 = scanner.nextInt();
					while (line3 != 0 && line3 != 1) {
						System.out.println("Enter choose number Department: ");
						line3 = scanner.nextInt();
					}
					switch (line3) {
					case 0:
						for (int i = 0; i < n; i++) {
							List<Result> list = new ArrayList<>();
							scanner.nextLine();
							System.out.print("Enter data id " + i + " : ");
							String id = Validator.inputTypeString();
//							String id = String.valueOf(scanner.nextLine());

							System.out.print("Enter name " + i + " : ");
							String name = String.valueOf(scanner.nextLine());
							while (!Validator.isFullname(name)) {
								System.out.println("Enter name again");
								name = scanner.nextLine();
							}

							System.out.print("Enter data birthday " + i + " : ");
							String birthday = scanner.nextLine();
							while (!Validator.isBirthDay(birthday)) {
								System.out.println("Enter date again");
								birthday = scanner.nextLine();
							}

							System.out.print("Enter data year " + i + " : ");
							int year = Validator.inputTypeInt();
//							int year = scanner.nextInt();
							scanner.nextLine();
							System.out.print("Enter data entry_point " + i + " : ");
							Double entry_point = Validator.inputTypeDouble();
//							Double entry_point = scanner.nextDouble();
							scanner.nextLine();
							System.out.print("Enter number of semester " + i + " : ");
							int number = Validator.inputTypeInt();
//							int number = scanner.nextInt();
							scanner.nextLine();
							list = managentStudent.foreachResults(number);
							Student sto = new Student(id, name, birthday, year, entry_point, list);
							listStudents.add(sto);
						}
						break;
					case 1:
						for (int i = 0; i < n; i++) {
							List<Result> list = new ArrayList<>();
							scanner.nextLine();
							System.out.print("Enter data id " + i + " : ");
							String id = Validator.inputTypeString();
//							String id = String.valueOf(scanner.nextLine());
							System.out.print("Enter name " + i + " : ");
							String name = String.valueOf(scanner.nextLine());
							while (!Validator.isFullname(name)) {
								System.out.println("Enter name again");
								name = scanner.nextLine();
							}
							System.out.print("Enter data birthday " + i + " : ");
							String birthday = scanner.nextLine();
							while (!Validator.isBirthDay(birthday)) {
								System.out.println("Enter date again");
								birthday = scanner.nextLine();
							}
							System.out.print("Enter data year: ");
							int year = Validator.inputTypeInt();
//							int year = scanner.nextInt();
							scanner.nextLine();
							System.out.print("Enter data entry_point " + i + " : ");
							Double entry_point = Validator.inputTypeDouble();
//							Double entry_point = scanner.nextDouble();
							scanner.nextLine();
							System.out.print("Enter number of semester " + i + " : ");
							int number = Validator.inputTypeInt();
//							int number = scanner.nextInt();
							scanner.nextLine();
							scanner.nextLine();
							System.out.print("Enter address: " + i + " : ");
							String address = Validator.inputTypeString();
//							String address = scanner.nextLine();
							list = managentStudent.foreachResults(number);
							Student sto = new Inoffice(id, name, birthday, year, entry_point, list, address);
							listStudents.add(sto);
						}
						break;
					default:
						System.out.println("Try it !!!");
						break;
					}

					managentStudent.addDepartment(name_dep, listStudents);
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}
			case "5": {
				try {
					System.out.print("Sum Student Official of Deparment: ");
					managentStudent.getSumStudentOfficial();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}

			case "6": {
				try {
					System.out.println("Get Student Has entrypoint Max in each Department: ");
					managentStudent.getStudentHasGradeMax();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}

			case "7": {
				try {
					System.out.println("Get Student Has entrypoint Max in each Department: ");
					System.out.print("Enter data address: ");
					String address = String.valueOf(scanner.nextLine());
					managentStudent.getStudentInofficeByAddressEachDerpartment(address);
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}

			case "8": {
				try {
					System.out.println("Get Student Has medium_score > 8 in latest Semester: ");
					managentStudent.getStudentHasGradeOver8();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}
			case "9": {
				try {
					System.out.println("Get Student Has medium_score max every semester : ");
					managentStudent.getStudentHasMediumMax();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}

			case "10": {
				try {
					System.out.println("list of number Student by year each Department: ");
					managentStudent.listStudentByYear();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}

			case "11": {
				try {
					System.out.println("Sorted data by Comparator: ");
					managentStudent.sortStudentByYearAndEntryPointComparator();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}
			case "12": {
				try {
					System.out.println("Sorted data by Comparable: ");
					managentStudent.sortStudentByYearAndEntryPointComparable();
				} catch (Exception ex) {
					// TODO: handle exception
					System.out.println(ex);
				}
				break;
			}
			case "13": {
				return;
			}
			default:
				System.out.println("Try it !!!");
				continue;
			}

		}
	}

}
