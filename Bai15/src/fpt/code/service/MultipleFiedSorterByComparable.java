package fpt.code.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fpt.code.entities.EntryPointSorter;
import fpt.code.entities.Inoffice;
import fpt.code.entities.Result;
import fpt.code.entities.Student;
import fpt.code.entities.YearSorter;

public class MultipleFiedSorterByComparable {
	public static void main(String[] args) {

		List<Result> rs = new ArrayList<>();
		Result rs1 = new Result(20181, 9.0);
		Result rs2 = new Result(20182, 8.0);
		rs.add(rs2);
		rs.add(rs1);
		List<Student> students = new ArrayList<>();
		Student st1 = new Inoffice("123", "duong01", "12-10-2000", 2018, 9.1, rs, "Hanoi");
		Student st2 = new Inoffice("456", "duong02", "12-11-2000", 2017, 8.1, rs, "Hanoi");
		Student st3 = new Inoffice("789", "duong03", "12-12-2000", 2016, 7.1, rs, "Hanoi");
		students.add(st1);
		students.add(st2);
		students.add(st3);

		System.out.println("Before sorting: ");
		for (Student student : students) {
			System.out.println(students);
		}
		Collections.sort(students);

		System.out.println("After sorting: ");
		for (Student student : students) {
			System.out.println(students);
		}
	}
}
