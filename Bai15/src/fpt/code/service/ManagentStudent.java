package fpt.code.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import fpt.code.entities.Department;
import fpt.code.entities.EntryPointSorter;
import fpt.code.entities.Inoffice;
import fpt.code.entities.Result;
import fpt.code.entities.Student;
import fpt.code.entities.YearSorter;

public class ManagentStudent {
	private List<Student> students;
	private Map<String, List<Student>> departments;

	public ManagentStudent() {
		this.students = new ArrayList<>();
		this.departments = new HashMap<>();
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Map<String, List<Student>> getDepartments() {
		return departments;
	}

	public void setDepartments(Map<String, List<Student>> departments) {
		this.departments = departments;
	}

	public void addStudent(Student st) {
		this.students.add(st);
	}

	public void addDepartment(String name, List<Student> students) {
		this.departments.put(name, students);
	}

	public boolean isOfficial(String id) {
		Student st = this.findById(id);
		if (st != null) {
			if (st instanceof Inoffice) {
				System.out.println("This is student Inofficial !!!");
				return false;
			} else {
				System.out.println("This is student Official ");
				return true;
			}
		} else
			System.out.println("Obj not exist !!!");
		return false;
	}

	public Student findById(String id) {
		return this.students.stream().filter(e -> e.getId().equals(id)).findFirst().orElse(null);
	}

	public List<Result> foreachResults(int number) {
		Scanner scanner = new Scanner(System.in);
		List<Result> results = new ArrayList<>();
		try {
			for (int j = 0; j < number; j++) {

				System.out.print("Enter semester " + j + " : ");
				int semester = scanner.nextInt();
				scanner.nextLine();
				System.out.print("Enter medium_score " + j + " : ");
				double medium_score = scanner.nextDouble();
				scanner.nextLine();
				results.add(new Result(semester, medium_score));
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return results;

	}

	public List<Result> getMediumScoreOfStudent(String id, int semester) {
		Student st = this.findById(id);
		if (st != null) {
			return st.getResults().stream().filter(o -> o.getSemester() == semester).collect(Collectors.toList());
		} else
			System.out.println("Object not exist !!!");
		return null;

	}

	public void getStudentHasGradeMax() {
		Map<String, Student> hashMap = new HashMap<>();
		Set<String> set = this.departments.keySet();
		List<String> arr = new ArrayList<>();
		for (String string : set) {
			arr.add(string);
		}
		int j = 0;
		for (List<Student> list : this.departments.values()) {
			Student max = list.stream().max(Comparator.comparing(Student::getEntry_point)).get();
			hashMap.put(arr.get(j), max);
			j++;
		}
		hashMap.entrySet().forEach((v) -> System.out.println(v.getKey() + " - " + v.getValue()));
	}

	public void getSumStudentOfficial() {
		Map<String, Long> count = new HashMap<>();
		Set<String> set = this.departments.keySet();
		List<String> arr = new ArrayList<>();
		for (String string : set) {
			arr.add(string);
		}
		int j = 0;
		for (List<Student> list : this.departments.values()) {
			Long i = list.stream().filter((st) -> (st instanceof Inoffice) == false).count();
			count.put(arr.get(j), i);
			j++;
		}
		count.entrySet().forEach((v) -> System.out.println(v.getKey() + " - " + v.getValue()));
	}

	public void getStudentInofficeByAddressEachDerpartment(String address) {
		Map<String, List<Student>> hashMap = new HashMap<>();
		Set<String> set = this.departments.keySet();
		List<String> arr = new ArrayList<>();

		for (String string : set) {
			arr.add(string);
		}
		int j = 0;
		for (List<Student> list : this.departments.values()) {
			List<Student> inoffices = new ArrayList<>();
			for (int i = 0; i < list.size(); i++) {
				Student inoffice = list.get(i);
				if (inoffice instanceof Inoffice && ((Inoffice) inoffice).getAddress().equals(address)) {
					inoffices.add(inoffice);
				}
			}
			hashMap.put(arr.get(j), inoffices);
			j++;
		}
		hashMap.entrySet().forEach((v) -> System.out.println(v.getKey() + " - " + v.getValue()));
	}

	public void getStudentHasGradeOver8() {
		Map<String, List<Student>> hashMap = new HashMap<>();
		Set<String> set = this.departments.keySet();
		List<String> arr = new ArrayList<>();

		for (String string : set) {
			arr.add(string);
		}
		int j = 0;
		for (List<Student> list : this.departments.values()) {
			List<Student> students = new ArrayList<>();
			for (int i = 0; i < list.size(); i++) {
				Student st = list.get(i);
				List<Result> rs = st.getResults();
				Result semester = rs.stream().max(Comparator.comparing(Result::getSemester)).get();
				if (rs.stream().filter(o -> o.getMedium_score() >= 8 && o.getSemester() == semester.getSemester())
						.count() > 0) {
					students.add(st);
				}
			}
			hashMap.put(arr.get(j), students);
			j++;

		}
		hashMap.entrySet().forEach((v) -> System.out.println(v.getKey() + " - " + v.getValue()));
	}

	public void getStudentHasMediumMax() {
		Map<String, List<Student>> hashMap = new HashMap<>();
		Set<String> set = this.departments.keySet();
		List<String> arr = new ArrayList<>();

		for (String string : set) {
			arr.add(string);
		}
		int j = 0;
		for (List<Student> list : this.departments.values()) {
			List<Student> students = new ArrayList<>();
			for (int i = 0; i < list.size(); i++) {
				Student st = list.get(i);
				List<Result> rs = st.getResults();
				Result semester = rs.stream().max(Comparator.comparing(Result::getMedium_score)).get();
				if (rs.stream().filter(o -> o.getMedium_score() == semester.getMedium_score()).count() > 0) {
					students.add(st);
				}
			}
			hashMap.put(arr.get(j), students);
			j++;

		}
		hashMap.entrySet().forEach((v) -> System.out.println(v.getKey() + " - " + v.getValue()));
	}

	public void listStudentByYear() {

		Map<String, List<Student>> hashMap = new HashMap<>();
		Set<String> set = this.departments.keySet();
		List<String> arr = new ArrayList<>();

		for (String string : set) {
			arr.add(string);
		}
		int j = 0;
		Set<Integer> number = new HashSet<>();
		for (List<Student> list : this.departments.values()) {
			List<Student> lists = new ArrayList<>();

			for (int i = 0; i < list.size(); i++) {
				Student st = list.get(i);
				number.add(st.getYear());
				lists.add(st);
			}

			hashMap.put(arr.get(j), lists);
			j++;
		}

		for (Integer integer : number) {
			int a = integer.intValue();
			for (Map.Entry<String, List<Student>> entry : hashMap.entrySet()) {
				String key = entry.getKey();
				List<Student> value = entry.getValue();
				Long i = value.stream().filter(o -> o.getYear() == a).count();
				if (i > 0) {
					System.out.println("ket qua: " + key + " - " + a + " - " + i);
				}

			}
		}

	}

	public void sortStudentByYearAndEntryPointComparator() {
		List<Result> rs = new ArrayList<>();
		Result rs1 = new Result(20181, 9.0);
		Result rs2 = new Result(20182, 8.0);
		rs.add(rs2);
		rs.add(rs1);
		List<Student> students = new ArrayList<>();
		Student st1 = new Inoffice("123", "duong01", "12-10-2000", 2018, 9.1, rs, "Hanoi");
		Student st2 = new Inoffice("456", "duong02", "12-11-2000", 2017, 8.1, rs, "Hanoi");
		Student st3 = new Inoffice("789", "duong03", "12-12-2000", 2016, 7.1, rs, "Hanoi");
		students.add(st1);
		students.add(st2);
		students.add(st3);

		System.out.println("Before sorting: ");
		for (Student student : students) {
			System.out.println(students);
		}
		Collections.sort(students, new EntryPointSorter().thenComparing(new YearSorter()));

		System.out.println("After sorting: ");
		for (Student student : students) {
			System.out.println(students);
		}

	}
	
	public void sortStudentByYearAndEntryPointComparable() {
		List<Result> rs = new ArrayList<>();
		Result rs1 = new Result(20181, 9.0);
		Result rs2 = new Result(20182, 8.0);
		rs.add(rs2);
		rs.add(rs1);
		List<Student> students = new ArrayList<>();
		Student st1 = new Inoffice("123", "duong01", "12-10-2000", 2018, 9.1, rs, "Hanoi");
		Student st2 = new Inoffice("456", "duong02", "12-11-2000", 2017, 8.1, rs, "Hanoi");
		Student st3 = new Inoffice("789", "duong03", "12-12-2000", 2016, 7.1, rs, "Hanoi");
		students.add(st1);
		students.add(st2);
		students.add(st3);

		System.out.println("Before sorting: ");
		for (Student student : students) {
			System.out.println(students);
		}
		Collections.sort(students);

		System.out.println("After sorting: ");
		for (Student student : students) {
			System.out.println(students);
		}

	}

}
