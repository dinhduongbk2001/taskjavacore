package fpt.code.utils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fpt.code.exception.BirthDayException;
import fpt.code.exception.FullNameException;

public class Validator {
	private static Scanner scanner = new Scanner(System.in);
	private static final String FULLNAME_REGEX = "^[a-zA-Z\\s]+";
	private static final String BIRTHDAY_REGEX = "^\\d{2}[-|/]\\d{2}[-|/]\\d{4}$";

	public static Boolean isBirthDay(String BirthDay) throws BirthDayException {
		Pattern pattern = Pattern.compile(BIRTHDAY_REGEX);
		Matcher matcher = pattern.matcher(BirthDay);

		if (matcher.matches()) {
			System.out.print("");
		} else {
			throw new BirthDayException("BirthDay ko hop le !!!");
		}
		return matcher.matches();
	}

	public static Boolean isFullname(String fullname) throws FullNameException {
		Pattern pattern = Pattern.compile(FULLNAME_REGEX);
		Matcher matcher = pattern.matcher(fullname);
		if (matcher.matches() == true && fullname.length() < 50) {
			System.out.print("");
		} else {
			throw new FullNameException("Fullname ko hop le !!!");
		}
		return matcher.matches();
	}

	public static int inputTypeInt() {
		int intValue = 0;
		do {
			String str = scanner.nextLine();
			try {
				intValue = Integer.parseInt(str);
				break;
			} catch (Exception e) {
				System.err.println("Please input int value!");
			}
		} while (true);
		return intValue;
	}

	public static double inputTypeDouble() {
		double DbValue = 0;
		do {
			String str = scanner.nextLine();
			try {
				DbValue = Double.parseDouble(str);
				break;
			} catch (Exception e) {
				System.err.println("Please input double value!");
			}
		} while (true);
		return DbValue;
	}

	public static String inputTypeString() {
		while (true) {
			String result = scanner.nextLine();
			if (result.isEmpty()) {
				System.err.println("Not empty!");
				System.out.print("Enter again: ");
			} else if (result.length() > 50) {
				System.err.println("Values must <= 50 characters!");
				System.out.print("Enter again: ");
			} else {
				return result;
			}
		}
	}

}
