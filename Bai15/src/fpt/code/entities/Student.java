package fpt.code.entities;

import java.util.Comparator;
import java.util.List;

public class Student implements Comparable<Student> {
	protected String id;
	protected String name;
	protected String birthday;
	protected int year;
	protected double entry_point;
	protected List<Result> results;

	public Student(String id, String name, String birthday, int year, double entry_point, List<Result> results) {
		super();
		this.id = id;
		this.name = name;
		this.birthday = birthday;
		this.year = year;
		this.entry_point = entry_point;
		this.results = results;
	}

	public Student() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getEntry_point() {
		return entry_point;
	}

	public void setEntry_point(double entry_point) {
		this.entry_point = entry_point;
	}

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", birthday=" + birthday + ", year=" + year + ", entry_point="
				+ entry_point + ", results=" + results + "]";
	}

	@Override
	public int compareTo(Student st) {
		int i = (int)(this.entry_point - st.getEntry_point());
		if (i != 0)
			return i;
		return  (this.year-st.getYear());
	}


}
