package fpt.code.entities;

import java.util.Comparator;

public class EntryPointSorter implements Comparator<Student> {
	public int compare(Student o1, Student o2) {
		return (int) (o1.getEntry_point() - o2.getEntry_point());
	}
}
