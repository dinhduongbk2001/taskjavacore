package fpt.code.entities;

import java.util.List;

public class Inoffice extends Student {
	private String address;

	public Inoffice(String id, String name, String birthday, int year, double entry_point, List<Result> results,
			String address) {
		super(id, name, birthday, year, entry_point, results);
		this.address = address;
	}

	public Inoffice() {

	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Inoffice [address=" + address + ", id=" + id + ", name=" + name + ", birthday=" + birthday + ", year="
				+ year + ", entry_point=" + entry_point + ", results=" + results + "]";
	}

}
