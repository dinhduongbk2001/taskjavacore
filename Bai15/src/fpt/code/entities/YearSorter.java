package fpt.code.entities;

import java.util.Comparator;

public class YearSorter implements Comparator<Student> 
{
	  public int compare(Student o1, Student o2) 
	    {
	        return (int) (o2.getYear() - o1.getYear());
	    }
}

