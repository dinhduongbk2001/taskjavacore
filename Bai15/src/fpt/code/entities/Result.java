package fpt.code.entities;

public class Result {
	private int semester;
	private double medium_score;

	public Result(int semester, double medium_score) {
		this.semester = semester;
		this.medium_score = medium_score;
	}

	public Result() {
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public double getMedium_score() {
		return medium_score;
	}

	public void setMedium_score(int medium_score) {
		this.medium_score = medium_score;
	}

	@Override
	public String toString() {
		return "Result [semester=" + semester + ", medium_score=" + medium_score + "]";
	}

}
