package fpt.code.entities;

import java.util.List;

public class Department {
	private String name_dep;
	private List<Student> students;

	public Department(String name_dep, List<Student> students) {
		super();
		this.name_dep = name_dep;
		this.students = students;
	}

	public Department() {

	}

	public String getName_dep() {
		return name_dep;
	}

	public void setName_dep(String name_dep) {
		this.name_dep = name_dep;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@Override
	public String toString() {
		return "Department [name_dep=" + name_dep + ", students=" + students + "]";
	}

}
