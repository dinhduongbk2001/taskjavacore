package fpt.code.main;

import java.util.Scanner;

import fpt.code.dao.ManageCanbo;
import fpt.code.entities.CanBo;
import fpt.code.entities.Congnhan;
import fpt.code.entities.KySu;
import fpt.code.entities.NhanVien;

public class ManagementProgram {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ManageCanbo managentCanbo = new ManageCanbo();
		while (true) {
			System.out.println("====== Manage Program======");
			System.out.println("1. add canbo ");
			System.out.println("2. To search canbo by name: ");
			System.out.println("3. display information list'canbo");
			System.out.println("4. close program Do it : ");
			String line = scanner.nextLine();
			switch (line) {
			case "1": {
				System.out.println("Enter a: to add kysu");
				System.out.println("Enter b: to add congnhan");
				System.out.println("Enter c: to add nhanvien");
				String type = scanner.nextLine();
				switch (type) {
				case "a": {
					System.out.print("Enter name: ");
					String name = scanner.nextLine();
					System.out.print("Enter age:");
					int age = scanner.nextInt();
					System.out.print("Enter gender: ");
					scanner.nextLine();
					String gender = scanner.nextLine();
					System.out.print("Enter address: ");
					String address = scanner.nextLine();
					System.out.print("Enter major: ");
					String major = scanner.nextLine();
					CanBo kysu = new KySu(name, age, gender, address, major);
					managentCanbo.addCanbo(kysu);
					System.out.println(kysu.toString());
					break;

				}
				case "b": {
					System.out.print("Enter name: ");
					String name = scanner.nextLine();
					System.out.print("Enter age:");
					int age = scanner.nextInt();
					System.out.print("Enter gender: ");
					scanner.nextLine();
					String gender = scanner.nextLine();
					System.out.print("Enter address: ");
					String address = scanner.nextLine();
					System.out.print("Enter level: ");
					int level = scanner.nextInt();
					boolean flag = true;
					while (flag) {
						if (level >= 0 && level <= 10) {
							flag = false;
						} else {
							System.out.print("Value level fail - ReEnter: ");
							level = scanner.nextInt();
						}
					}
					Congnhan cn = new Congnhan(name, age, gender, address, level);
					managentCanbo.addCanbo(cn);
					System.out.println(cn.toString());
					scanner.nextLine();
					break;
				}
				case "c": {
					System.out.print("Enter name: ");
					String name = scanner.nextLine();
					System.out.print("Enter age: ");
					int age = scanner.nextInt();
					System.out.print("Enter gender: ");
					scanner.nextLine();
					String gender = scanner.nextLine();
					System.out.print("Enter address: ");
					String address = scanner.nextLine();
					System.out.print("Enter job: ");
					String job = scanner.nextLine();
					NhanVien nv = new NhanVien(name, age, gender, address, job);
					managentCanbo.addCanbo(nv);
					System.out.println(nv.toString());
					break;
				}
				default:
					System.out.println("Try it !!!");
					break;
				}
				break;
			}
			case "2": {
				System.out.print("Enter name to search: ");
				String name = scanner.nextLine();
				managentCanbo.searchByName(name).forEach(obj -> {
					System.out.println(obj.toString());
				});
				break;
			}
			case "3": {
				managentCanbo.diplayListCanbo();
				break;
			}
			case "4": {
				return;
			}
			default:
				System.out.println("Try it !!!");
				continue;
			}

		}
	}

}