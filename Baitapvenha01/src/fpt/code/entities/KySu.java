package fpt.code.entities;

public class KySu extends CanBo {
	private String major;

	public KySu(String name, int age, String gender, String address, String major) {
		super(name, age, gender, address);
		this.major = major;
	}

	public String getMajo() {
		return major;
	}

	public void setMajo(String majo) {
		this.major = major;
	}

	@Override
	public String toString() {
		return "KySu [majo=" + major + ", getName()=" + getName() + ", getAge()=" + getAge() + ", getGender()="
				+ getGender() + ", getAddress()=" + getAddress();
	}

}
