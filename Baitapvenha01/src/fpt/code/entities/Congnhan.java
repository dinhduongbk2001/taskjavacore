package fpt.code.entities;

public class Congnhan extends CanBo {
	private int level;

	public Congnhan(String name, int age, String gender, String address, int level) {
		super(name, age, gender, address);
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "Congnhan [level=" + level + ", getName()=" + getName() + ", getAge()=" + getAge() + ", getGender()="
				+ getGender() + ", getAddress()=" + getAddress();
	}

}
