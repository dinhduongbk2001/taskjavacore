package fpt.code.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import fpt.code.entities.CanBo;

public class ManageCanbo {
	private List<CanBo> list;

	public ManageCanbo() {
		this.list = new ArrayList<>();
	}

	public void addCanbo(CanBo cb) {
		this.list.add(cb);
	}

	public List<CanBo> searchByName(String name) {
		return this.list.stream().filter((obj) -> obj.getName().contains(name)).collect(Collectors.toList());

	}

	public void diplayListCanbo() {
		this.list.forEach((obj) -> System.out.println(obj.toString()));

	}

}
