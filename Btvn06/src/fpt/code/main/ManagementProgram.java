package fpt.code.main;

import java.util.Scanner;

import fpt.code.entities.Classroom;
import fpt.code.entities.Student;

public class ManagementProgram {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Classroom managentStudent = new Classroom();
		while (true) {
			System.out.println("====== Manage Program======");
			System.out.println("1. add student ");
			System.out.println("2. display information list'student by age 20 yearolds");
			System.out.println("3. get count number of student have age by 23 and address is DN");
			System.out.println("4. close program Do it : ");
			String line = scanner.nextLine();
			switch (line) {
			case "1": {
				System.out.print("Enter name: ");
				String name = scanner.nextLine();
				System.out.print("Enter age:");
				int age = scanner.nextInt();
				scanner.nextLine();
				System.out.print("Enter address: ");
				String address = scanner.nextLine();
				Student st = new Student(name, age, address);
				managentStudent.addStudent(st);
				System.out.println(st.toString());
				break;
			}
			case "2": {
				managentStudent.displayListStudentAgeBy20().forEach(obj -> System.out.println(obj));
				break;
			}
			case "3": {
				long count = 0;
				count = managentStudent.getCountStudentAgeBy23AndAddressByDN();
				System.out.println("count: " + count);
				break;
			}
			case "4": {
				return;
			}
			default:
				System.out.println("Try it !!!");
				continue;
			}

		}
	}
}
