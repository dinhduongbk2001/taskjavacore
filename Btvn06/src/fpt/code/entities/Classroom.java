package fpt.code.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Classroom {
	private List<Student> list;

	public Classroom() {
		this.list = new ArrayList<>();
	}

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}

	public void addStudent(Student st) {
		this.list.add(st);
	}

	public List<Student> displayListStudentAgeBy20() {
		return this.list.stream().filter(obj -> (obj.getAge() == 20)).collect(Collectors.toList());
	}

	public long getCountStudentAgeBy23AndAddressByDN() {
		return this.list.stream().filter(obj -> (obj.getAge() == 23 && obj.getAddress().equals("DN"))).count();
	}

}
