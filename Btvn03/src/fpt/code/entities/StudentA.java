package fpt.code.entities;

public class StudentA extends Candidate {
	public static final String MATH = "Toan";
	public static final String PHYSICS = "Ly";
	public static final String CHEMISTRY = "Hoa";

	public StudentA(int sbd, String name, String address, int priority) {
		super(sbd, name, address, priority);
	}

	@Override
	public String toString() {
		return "StudentA [getSbd()=" + getSbd() + ", getName()=" + getName() + ", getAddress()=" + getAddress()
				+ ", getPriority()=" + getPriority() + "ExamSubject: " + MATH + " - " + PHYSICS + " - " + CHEMISTRY;
	}

}
