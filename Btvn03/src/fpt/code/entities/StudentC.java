package fpt.code.entities;

public class StudentC extends Candidate {

	public static final String LITERATURE = "Van";
	public static final String HISTORY = "Su";
	public static final String GEOGRAPHY = "Dia";

	public StudentC(int sbd, String name, String address, int priority) {
		super(sbd, name, address, priority);
	}

	@Override
	public String toString() {
		return "StudentC [getSbd()=" + getSbd() + ", getName()=" + getName() + ", getAddress()=" + getAddress()
				+ ", getPriority()=" + getPriority() + "]" + "ExamSubject: " + LITERATURE + " - " + HISTORY + " - "
				+ GEOGRAPHY;
	}

}
