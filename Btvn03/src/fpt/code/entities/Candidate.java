package fpt.code.entities;

public class Candidate {
	private int sbd;
	private String name;
	private String address;
	private int priority;

	public Candidate(int sbd, String name, String address, int priority) {
		super();
		this.sbd = sbd;
		this.name = name;
		this.address = address;
		this.priority = priority;
	}

	public Candidate() {
		super();
	}

	public int getSbd() {
		return sbd;
	}

	public void setSbd(int sbd) {
		this.sbd = sbd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "Candidate [sbd=" + sbd + ", name=" + name + ", address=" + address + ", priority=" + priority + "]";
	}

}
