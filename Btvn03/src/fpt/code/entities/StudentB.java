package fpt.code.entities;

public class StudentB extends Candidate {
	public static final String MATH = "Toan";
	public static final String CHEMISTRY = "Hoa";
	public static final String BIOLOGY = "Sinh";

	public StudentB(int sbd, String name, String address, int priority) {
		super(sbd, name, address, priority);
	}

	@Override
	public String toString() {
		return "StudentB [getSbd()=" + getSbd() + ", getName()=" + getName() + ", getAddress()=" + getAddress()
				+ ", getPriority()=" + getPriority() + "]" + "ExamSubject: " + MATH + " - " + CHEMISTRY + " - "
				+ BIOLOGY;
	}

}
