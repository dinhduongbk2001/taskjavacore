package fpt.code.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fpt.code.entities.Candidate;

public class ManageStudent {
	private List<Candidate> listStudent;

	public ManageStudent() {
		this.listStudent = new ArrayList<>();
	}

	public void addStudent(Candidate can) {
		this.listStudent.add(can);
	}

	public List<Candidate> displayListStudent() {
		return this.listStudent;
	}

	public List<Candidate> searchStudentBySbd(int sbd) {
		return this.listStudent.stream().filter((st) -> st.getSbd() == sbd).collect(Collectors.toList());
	}

}
