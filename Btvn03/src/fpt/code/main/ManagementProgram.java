package fpt.code.main;

import java.util.Scanner;

import fpt.code.dao.ManageStudent;
import fpt.code.entities.Candidate;
import fpt.code.entities.StudentA;
import fpt.code.entities.StudentB;
import fpt.code.entities.StudentC;

public class ManagementProgram {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ManageStudent managentStudent = new ManageStudent();
		while (true) {
			System.out.println("====== Manage Program======");
			System.out.println("1. add student ");
			System.out.println("2. display information list'student");
			System.out.println("3. To search student by sbd:");
			System.out.println("4. close program Do it : ");
			String line = scanner.nextLine();
			switch (line) {
			case "1": {
				System.out.println("Enter a: to add StudentA");
				System.out.println("Enter b: to add StudentB");
				System.out.println("Enter c: to add StudentC");
				String type = scanner.nextLine();
				switch (type) {
				case "a": {
					System.out.print("Enter sbd:");
					int sbd = scanner.nextInt();
					System.out.print("Enter name: ");
					scanner.nextLine();
					String name = scanner.nextLine();
					System.out.print("Enter address: ");
					String address = scanner.nextLine();
					System.out.print("Enter priority: ");
					int priority = scanner.nextInt();
					Candidate stA = new StudentA(sbd, name, address, priority);
					managentStudent.addStudent(stA);
					System.out.println(stA.toString());
					break;

				}
				case "b": {
					System.out.print("Enter sbd:");
					int sbd = scanner.nextInt();
					System.out.print("Enter name: ");
					scanner.nextLine();
					String name = scanner.nextLine();
					System.out.print("Enter address: ");
					String address = scanner.nextLine();
					System.out.print("Enter priority: ");
					int priority = scanner.nextInt();
					Candidate stB = new StudentB(sbd, name, address, priority);
					managentStudent.addStudent(stB);
					System.out.println(stB.toString());
					break;
				}
				case "c": {
					System.out.print("Enter sbd:");
					int sbd = scanner.nextInt();
					System.out.print("Enter name: ");
					scanner.nextLine();
					String name = scanner.nextLine();
					System.out.print("Enter address: ");
					String address = scanner.nextLine();
					System.out.print("Enter priority: ");
					int priority = scanner.nextInt();
					Candidate stC = new StudentC(sbd, name, address, priority);
					managentStudent.addStudent(stC);
					System.out.println(stC.toString());
					break;
				}
				default:
					System.out.println("Try it !!!");
					break;
				}
				break;
			}
			case "3": {

				try {
					System.out.print("Enter sbd to search: ");
					String sbd = scanner.nextLine();
					int intValue = 0;
					intValue = Integer.parseInt(sbd);
					managentStudent.searchStudentBySbd(intValue).forEach(obj -> {
						System.out.println(obj.toString());
					});
				} catch (Exception ex) {
					System.out.println(ex);
				}
				break;
			}
			case "2": {
				managentStudent.displayListStudent().forEach(obj -> System.out.println(obj));
				break;
			}
			case "4": {
				return;
			}
			default:
				System.out.println("Try it !!!");
				continue;
			}

		}
	}

}