package fpt.code.entities;

import java.util.ArrayList;
import java.util.List;

public class HoGiaDinh {
	private List<Nguoi> listNguoi;
	private String address;

	public HoGiaDinh() {
		this.listNguoi = new ArrayList<>();
	}

	public HoGiaDinh(List<Nguoi> listNguoi, String address) {
		super();
		this.listNguoi = listNguoi;
		this.address = address;
	}

	public List<Nguoi> getListPerson() {
		return listNguoi;
	}

	public void setListPerson(List<Nguoi> listPerson) {
		this.listNguoi = listPerson;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void addNguoi(Nguoi n) {
		this.listNguoi.add(n);
	}

	@Override
	public String toString() {
		return "HoGiaDinh [listNguoi=" + listNguoi + ", address=" + address + "]";
	}

}
