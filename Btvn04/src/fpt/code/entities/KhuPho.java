package fpt.code.entities;

import java.util.ArrayList;
import java.util.List;

public class KhuPho {
	private List<HoGiaDinh> listHo;

	public KhuPho() {
		this.listHo = new ArrayList<>();
	}

	public List<HoGiaDinh> getListHo() {
		return listHo;
	}

	public void setListHo(List<HoGiaDinh> listHo) {
		this.listHo = listHo;
	}

	public KhuPho(List<HoGiaDinh> listHo) {
		super();
		this.listHo = listHo;
	}

	public void addHoGiDinh(HoGiaDinh hdh) {
		this.listHo.add(hdh);
	}

	@Override
	public String toString() {
		return "KhuPho [listHo=" + listHo + "]";
	}

}
