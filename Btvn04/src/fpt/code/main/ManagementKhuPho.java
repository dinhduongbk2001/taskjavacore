package fpt.code.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import fpt.code.entities.HoGiaDinh;
import fpt.code.entities.KhuPho;
import fpt.code.entities.Nguoi;

public class ManagementKhuPho {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		KhuPho kp = new KhuPho();

		while (true) {
			System.out.println("====== Manage Program======");
			System.out.println("1. add Ho dan ");
			System.out.println("2. display information list'Ho dan");
			System.out.println("3. close program Do it : ");
			String line = sc.nextLine();
			switch (line) {
			case "1": {
				System.out.println("Enter of number Ho dan: ");
				int n = sc.nextInt();
				sc.nextLine();

				for (int i = 0; i < n; i++) {

					System.out.println("Enter address of family " + i + " : ");
					String address = sc.nextLine();
					List<Nguoi> listNguoi = new ArrayList<>();
					System.out.println("Enter number of person in this family " + i + " : ");
					int number = sc.nextInt();
					sc.nextLine();

					for (int j = 0; j < number; j++) {
						System.out.print("Enter name " + j + ": ");
						String name = sc.nextLine();
						System.out.print("Enter age " + j + " : ");
						int age = sc.nextInt();
						System.out.print("Enter job " + j + " : ");
						sc.nextLine();
						String job = sc.nextLine();
						System.out.print("Enter id " + j + " : ");
						int id = sc.nextInt();
						sc.nextLine();
						listNguoi.add(new Nguoi(name, age, job, id));
					}

					kp.addHoGiDinh(new HoGiaDinh(listNguoi, address));
				}
				break;
			}

			case "2": {
				List<HoGiaDinh> list = kp.getListHo();
				for (HoGiaDinh hoGiaDinh : list) {
					System.out.println(hoGiaDinh.toString());
				}
				break;
			}
			case "3": {
				return;
			}
			default:
				System.out.println("Try it !!!");
				continue;
			}

		}

	}
}
