package fpt.code.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import fpt.code.utils.Validator;
import fpt.code.entities.Certificate;
import fpt.code.entities.Employee;
import fpt.code.entities.Experience;
import fpt.code.entities.Fresher;
import fpt.code.entities.Intern;

public class ManageEmployee {

	private List<Employee> emps;

	public ManageEmployee() {
		this.emps = new ArrayList<>();
	}

	public void addEmployee(Employee emp) {
		this.emps.add(emp);
	}

	public Employee findById(String id) {
		return this.emps.stream().filter(e -> e.getId().equals(id)).findFirst().orElse(null);
	}

	public boolean deleteById(String id) {
		Employee emp = this.findById(id);
		if (emp == null) {
			System.out.println("No exist this employee !!!");
			return false;
		} else {
			this.emps.remove(emp);
			System.out.println("Elemement deleted !!!");
			return true;
		}
	}

	public List<Employee> findByType(int type) {
		List<Employee> list = new ArrayList<>();
		for (Employee employee : this.emps) {
			if (type == 0 && employee instanceof Experience) {
				list.add(employee);
				return list;
			} else if (type == 1 && employee instanceof Fresher ) {
				list.add(employee);
				return list;
			} else if (type == 2 && employee instanceof Intern) {
				list.add(employee);
				return list;
			} else {
				return new ArrayList<>();
			}
		}
		return list;
	}

	public Employee UpdateEmployee(String id) {
		Scanner scanner = new Scanner(System.in);
		try {

			Employee emp = this.emps.stream().filter(e -> e.getId().equals(id)).findFirst().orElse(null);
			List<Certificate> cers = emp.getCertificates();
			int number = emp.getCertificates().size();
			boolean flag = true;
			while (flag) {
				if (number <= 0) {
					System.out.println("please reEnter number of Certificate: ");
					number = scanner.nextInt();
				} else {
					flag = false;
				}
			}
			if (emp != null && emp.getType() == 0) {
				System.out.println("You will update employee Experience: ");
				int type = 0;
				System.out.print("Enter data fullname: ");
				String fullname = String.valueOf(scanner.nextLine());
				while (!Validator.isFullname(fullname)) {
					System.out.println("Enter fullname again");
					fullname = scanner.nextLine();
				}
				System.out.print("Enter data birthday: ");
				String birthday = scanner.nextLine();
				while (!Validator.isBirthDay(birthday)) {
					System.out.println("Enter date again");
					birthday = scanner.nextLine();
				}
				System.out.print("Enter data phone: ");
				String phone = String.valueOf(scanner.nextLine());
				while (!Validator.isPhone(phone)) {
					System.out.println("Enter phone again");
					phone = scanner.nextLine();
				}
				System.out.print("Enter data email: ");
				String email = String.valueOf(scanner.nextLine());
				while (!Validator.isEmail(email)) {
					System.out.println("Enter email again");
					email = scanner.nextLine();
				}
				System.out.print("Enter data expInYear: ");
				int expInYear = scanner.nextInt();
				scanner.nextLine();
				System.out.print("Enter data proSkill: ");
				String proSkill = String.valueOf(scanner.nextLine());
				;

				foreachCers(number);
				Employee experience = new Experience(emp.getId(), fullname, birthday, phone, email, cers, type,
						expInYear, proSkill);
				this.emps.remove(emp);
				this.emps.add(experience);

				return experience;
			} else if (emp != null && emp.getType() == 1) {
				System.out.println("You will update employee Fresher: ");
				int type = 1;
				System.out.print("Enter data fullname: ");
				String fullname = String.valueOf(scanner.nextLine());
				while (!Validator.isFullname(fullname)) {
					System.out.println("Enter fullname again");
					fullname = scanner.nextLine();
				}
				System.out.print("Enter data birthday: ");
				String birthday = scanner.nextLine();
				while (!Validator.isBirthDay(birthday)) {
					System.out.println("Enter date again");
					birthday = scanner.nextLine();
				}
				System.out.print("Enter data phone: ");
				String phone = String.valueOf(scanner.nextLine());
				while (!Validator.isPhone(phone)) {
					System.out.println("Enter phone again");
					phone = scanner.nextLine();
				}
				System.out.print("Enter data email: ");
				String email = String.valueOf(scanner.nextLine());
				while (!Validator.isEmail(email)) {
					System.out.println("Enter email again");
					email = scanner.nextLine();
				}
				System.out.print("Enter data education: ");
				String education = String.valueOf(scanner.nextLine());
				scanner.nextLine();
				System.out.print("Enter data graduation_rank: ");
				String graduation_rank = String.valueOf(scanner.nextLine());
				System.out.print("Enter data graduation_date: ");
				String graduation_date = String.valueOf(scanner.nextLine());
				foreachCers(number);
				Employee fresher = new Fresher(emp.getId(), fullname, birthday, phone, email, cers, type,
						graduation_date, graduation_rank, education);
				this.emps.remove(emp);
				this.emps.add(fresher);

				return fresher;
			} else if (emp != null && emp.getType() == 2) {
				System.out.println("You will update employee Intern: ");
				int type = 2;
				System.out.print("Enter data fullname: ");
				String fullname = String.valueOf(scanner.nextLine());
				while (!Validator.isFullname(fullname)) {
					System.out.println("Enter fullname again");
					fullname = scanner.nextLine();
				}
				System.out.print("Enter data birthday: ");
				String birthday = scanner.nextLine();
				while (!Validator.isBirthDay(birthday)) {
					System.out.println("Enter date again");
					birthday = scanner.nextLine();
				}
				System.out.print("Enter data phone: ");
				String phone = String.valueOf(scanner.nextLine());
				while (!Validator.isPhone(phone)) {
					System.out.println("Enter phone again");
					phone = scanner.nextLine();
				}
				System.out.print("Enter data email: ");
				String email = String.valueOf(scanner.nextLine());
				while (!Validator.isEmail(email)) {
					System.out.println("Enter email again");
					email = scanner.nextLine();
				}
				System.out.print("Enter majors : ");
				String majors = scanner.nextLine();
				System.out.print("Enter semester: ");
				int semester = scanner.nextInt();
				System.out.print("Enter university: ");
				String university = scanner.nextLine();
				this.emps.remove(emp);

				foreachCers(number);

				Employee intern = new Intern(emp.getId(), fullname, birthday, phone, email, cers, type, majors,
						semester, university);
				this.emps.remove(emp);
				this.emps.add(intern);
				return intern;
			} else
				return null;

		} catch (Exception e) {
			System.out.println("Update line failed !!!");
			e.printStackTrace();
			return null;
		}
	}

	public List<Certificate> foreachCers(int number) {
		Scanner scanner = new Scanner(System.in);
		List<Certificate> cers = new ArrayList<>();
		try {
			for (int j = 1; j <= number; j++) {
				System.out.print("Enter certificatedID " + j + " : ");
				int certificatedID = scanner.nextInt();
				scanner.nextLine();
				System.out.print("Enter certificateRank " + j + " : ");
				int certificateRank = scanner.nextInt();
				scanner.nextLine();
				System.out.print("Enter CertificateName " + j + " : ");
				String CertificateName = scanner.nextLine();
				System.out.print("Enter CertificatedDate " + j + " : ");
				String CertificatedDate = scanner.nextLine();
				while (!Validator.isBirthDay(CertificatedDate)) {
					System.out.println("Enter date again");
					CertificatedDate = scanner.nextLine();

				}
				cers.add(new Certificate(certificatedID, CertificateName, certificateRank, CertificatedDate));
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
		return cers;

	}

}
