package fpt.code.entities;

import java.time.LocalDate;
import java.util.List;

public class Fresher extends Employee {
	private String graduation_date;
	private String graduation_rank;
	private String education;

	public Fresher() {
	}

	public Fresher(String id, String fullname, String birthday, String phone, String email,
			List<Certificate> certificates, int type, String graduation_date, String graduation_rank,
			String education) {
		super(id, fullname, birthday, phone, email, certificates, type);
		this.graduation_date = graduation_date;
		this.graduation_rank = graduation_rank;
		this.education = education;
	}

	public String getGraduation_date() {
		return graduation_date;
	}

	public void setGraduation_date(String graduation_date) {
		this.graduation_date = graduation_date;
	}

	public String getGraduation_rank() {
		return graduation_rank;
	}

	public void setGraduation_rank(String graduation_rank) {
		this.graduation_rank = graduation_rank;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	@Override
	public void showInfor() {
		// TODO Auto-generated method stub
		this.toString();
	}

	@Override
	public String toString() {
		return "Fresher [graduation_date=" + graduation_date + ", graduation_rank=" + graduation_rank + ", education="
				+ education + ", id=" + id + ", fullname=" + fullname + ", birthday=" + birthday + ", phone=" + phone
				+ ", email=" + email + ", certificates=" + certificates + ", type=" + type + "]";
	}

}
