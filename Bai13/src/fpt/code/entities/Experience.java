package fpt.code.entities;

import java.time.LocalDate;
import java.util.List;

public class Experience extends Employee {
	private int expInYear;
	private String proSkill;

	public Experience(String id, String fullname, String birthday, String phone, String email,
			List<Certificate> certificates, int type, int expInYear, String proSkill) {
		super(id, fullname, birthday, phone, email, certificates, type);
		this.expInYear = expInYear;
		this.proSkill = proSkill;
	}

	public Experience() {

	}

	public int getExpInYear() {
		return expInYear;
	}

	public void setExpInYear(int expInYear) {
		this.expInYear = expInYear;
	}

	public String getProSkill() {
		return proSkill;
	}

	public void setProSkill(String proSkill) {
		this.proSkill = proSkill;
	}

	@Override
	public void showInfor() {
		// TODO Auto-generated method stub
		this.toString();
	}

	@Override
	public String toString() {
		return "Experience [expInYear=" + expInYear + ", proSkill=" + proSkill + ", id=" + id + ", fullname=" + fullname
				+ ", birthday=" + birthday + ", phone=" + phone + ", email=" + email + ", certificates=" + certificates
				+ ", type=" + type + "]";
	}

}
