package fpt.code.entities;

import java.time.LocalDate;
import java.util.List;

public class Intern extends Employee {
	private String majors;
	private int semester;
	private String university;

	@Override
	public void showInfor() {
		// TODO Auto-generated method stub
		this.toString();
	}

	public String getMajors() {
		return majors;
	}

	public void setMajors(String majors) {
		this.majors = majors;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public Intern(String id, String fullname, String birthday, String phone, String email,
			List<Certificate> certificates, int type, String majors, int semester, String university) {
		super(id, fullname, birthday, phone, email, certificates, type);
		this.majors = majors;
		this.semester = semester;
		this.university = university;
	}

	public Intern() {
	}

	@Override
	public String toString() {
		return "Intern [majors=" + majors + ", semester=" + semester + ", university=" + university + ", id=" + id
				+ ", fullname=" + fullname + ", birthday=" + birthday + ", phone=" + phone + ", email=" + email
				+ ", certificates=" + certificates + ", type=" + type + "]";
	}

}
