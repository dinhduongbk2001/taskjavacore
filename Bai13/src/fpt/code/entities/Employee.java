package fpt.code.entities;

import java.time.LocalDate;
import java.util.List;

public abstract class Employee {
	protected String id;
	protected String fullname;
	protected String birthday;
	protected String phone;
	protected String email;
	protected List<Certificate> certificates;
	protected int type;
	public static long count = 0;

	public Employee(String id, String fullname, String birthday, String phone, String email,
			List<Certificate> certificates, int type) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.birthday = birthday;
		this.phone = phone;
		this.email = email;
		this.certificates = certificates;
		this.type = type;
	}

	public Employee() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}

	public static long getCount() {
		return count;
	}

	public static void setCount(long count) {
		Employee.count = count;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", fullname=" + fullname + ", birthday=" + birthday + ", phone=" + phone
				+ ", email=" + email + ", certificates=" + certificates + ", type=" + type + "]";
	}

	public abstract void showInfor();
}
