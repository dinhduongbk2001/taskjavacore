package fpt.code.entities;

import java.time.LocalDate;

public class Certificate {
	private int certificatedID;
	private String CertificateName;
	private int certificateRank;
	private String CertificatedDate;

	public int getCertificatedID() {
		return certificatedID;
	}

	public void setCertificatedID(int certificatedID) {
		this.certificatedID = certificatedID;
	}

	public String getCertificateName() {
		return CertificateName;
	}

	public void setCertificateName(String certificateName) {
		CertificateName = certificateName;
	}

	public int getCertificateRank() {
		return certificateRank;
	}

	public void setCertificateRank(int certificateRank) {
		this.certificateRank = certificateRank;
	}

	public String getCertificatedDate() {
		return CertificatedDate;
	}

	public void setCertificatedDate(String certificatedDate) {
		CertificatedDate = certificatedDate;
	}

	public Certificate(int certificatedID, String certificateName, int certificateRank, String certificatedDate) {
		super();
		this.certificatedID = certificatedID;
		CertificateName = certificateName;
		this.certificateRank = certificateRank;
		CertificatedDate = certificatedDate;
	}

	public Certificate() {

	}

	@Override
	public String toString() {
		return "Certificate [certificatedID=" + certificatedID + ", CertificateName=" + CertificateName
				+ ", certificateRank=" + certificateRank + ", CertificatedDate=" + CertificatedDate + "]";
	}

}
