package fpt.code.exception;

public class BirthDayException extends Exception {
	public BirthDayException(String str) {
		super(str);
	}
}
