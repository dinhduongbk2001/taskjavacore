package fpt.code.exception;

public class PhoneException extends Exception {
	public PhoneException(String str) {
		super(str);
	}
}