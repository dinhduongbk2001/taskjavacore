package fpt.code.exception;

public class EmailException extends Exception {
	public EmailException(String str) {
		super(str);
	}
}
