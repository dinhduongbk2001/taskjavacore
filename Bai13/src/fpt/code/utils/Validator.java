package fpt.code.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fpt.code.exception.BirthDayException;
import fpt.code.exception.EmailException;
import fpt.code.exception.FullNameException;
import fpt.code.exception.PhoneException;

public class Validator {
	private static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	private static final String FULLNAME_REGEX = "^[a-zA-Z\\s]+";
	private static final String PHONE_REGEX = "(?:\\d.*){7,}";
	private static final String BIRTHDAY_REGEX = "^\\d{2}[-|/]\\d{2}[-|/]\\d{4}$";

	public static boolean isEmail(String email) throws EmailException {
		Pattern pattern = Pattern.compile(EMAIL_REGEX);
		Matcher matcher = pattern.matcher(email);

		if (matcher.matches()) {
			System.out.print("");
		} else {
			throw new EmailException("Email ko hop le !!!");
		}
		return matcher.matches();
	}

	public static Boolean isPhone(String phone) throws PhoneException {
		Pattern pattern = Pattern.compile(PHONE_REGEX);
		Matcher matcher = pattern.matcher(phone);

		if (matcher.matches()) {
			System.out.print("");
		} else {
			throw new PhoneException("Phone ko hop le !!!");
		}
		return matcher.matches();
	}

	public static Boolean isBirthDay(String BirthDay) throws BirthDayException {
		Pattern pattern = Pattern.compile(BIRTHDAY_REGEX);
		Matcher matcher = pattern.matcher(BirthDay);

		if (matcher.matches()) {
			System.out.print("");
		} else {
			throw new BirthDayException("BirthDay ko hop le !!!");
		}
		return matcher.matches();
	}

	public static Boolean isFullname(String fullname) throws FullNameException {
		Pattern pattern = Pattern.compile(FULLNAME_REGEX);
		Matcher matcher = pattern.matcher(fullname);
		if (matcher.matches() == true && fullname.length() < 50) {
			System.out.print("");
		} else {
			throw new FullNameException("Fullname ko hop le !!!");
		}
		return matcher.matches();
	}
}
