package fpt.code.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fpt.code.entities.Certificate;
import fpt.code.entities.Employee;
import fpt.code.entities.Experience;
import fpt.code.entities.Fresher;
import fpt.code.entities.Intern;
import fpt.code.service.ManageEmployee;
import fpt.code.utils.Validator;

public class ManagementProgram {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ManageEmployee managentEmp = new ManageEmployee();
		while (true) {
			System.out.println("====== Manage Program======");
			System.out.println("1. add Employee ");
			System.out.println("2. Find emps by type: ");
			System.out.println("3. Find emp by id: ");
			System.out.println("4. Delete emp by id: ");
			System.out.println("5. Update emp by id: ");
			System.out.println("7. close program Do it : ");
			String line = scanner.nextLine();
			switch (line) {
			case "1": {
				System.out.println("Enter a: to add Exp");
				System.out.println("Enter b: to add Fresher");
				System.out.println("Enter c: to add Inter");
				String type = scanner.nextLine();
				switch (type) {
				case "a": {
					try {
						System.out.println("You will add employee Experience: ");
						List<Certificate> list = new ArrayList<>();
						System.out.println();
						System.out.print("Enter data id: ");
						String id = String.valueOf(scanner.nextLine());
						System.out.print("Enter data fullname: ");
						String fullname = String.valueOf(scanner.nextLine());
						while (!Validator.isFullname(fullname)) {
							System.out.println("Enter fullname again");
							fullname = scanner.nextLine();
						}
						System.out.print("Enter data birthday: ");
						String birthday = scanner.nextLine();
						while (!Validator.isBirthDay(birthday)) {
							System.out.println("Enter date again");
							birthday = scanner.nextLine();
						}
						System.out.print("Enter data phone: ");
						String phone = String.valueOf(scanner.nextLine());
						while (!Validator.isPhone(phone)) {
							System.out.println("Enter phone again");
							phone = scanner.nextLine();
						}
						System.out.print("Enter data email: ");
						String email = String.valueOf(scanner.nextLine());
						while (!Validator.isEmail(email)) {
							System.out.println("Enter email again");
							email = scanner.nextLine();
						}
						System.out.print("Enter data expInYear: ");
						int expInYear = scanner.nextInt();
						scanner.nextLine();
						System.out.print("Enter data proSkill: ");
						String proSkill = String.valueOf(scanner.nextLine());
						System.out.print("Enter number bangcap: ");
						int number = scanner.nextInt();
						scanner.nextLine();
						list = managentEmp.foreachCers(number);
						Employee experience = new Experience(id, fullname, birthday, phone, email, list, 0, expInYear,
								proSkill);
						managentEmp.addEmployee(experience);
						System.out.println(experience.toString());
					} catch (Exception ex) {
						// TODO: handle exception
						System.out.println(ex);
					}

					break;
				}
				case "b": {
					try {
						System.out.println("You will add employee Fresher: ");
						List<Certificate> list = new ArrayList<>();
						System.out.println();
						System.out.print("Enter data id: ");
						String id = String.valueOf(scanner.nextLine());
						System.out.print("Enter data fullname: ");
						String fullname = String.valueOf(scanner.nextLine());
						while (!Validator.isFullname(fullname)) {
							System.out.println("Enter fullname again");
							fullname = scanner.nextLine();
						}
						System.out.print("Enter data birthday: ");
						String birthday = scanner.nextLine();
						while (!Validator.isBirthDay(birthday)) {
							System.out.println("Enter date again");
							birthday = scanner.nextLine();
						}
						System.out.print("Enter data phone: ");
						String phone = String.valueOf(scanner.nextLine());
						while (!Validator.isPhone(phone)) {
							System.out.println("Enter phone again");
							phone = scanner.nextLine();
						}
						System.out.print("Enter data email: ");
						String email = String.valueOf(scanner.nextLine());
						while (!Validator.isEmail(email)) {
							System.out.println("Enter email again");
							email = scanner.nextLine();
						}
						System.out.print("Enter data education: ");
						String education = String.valueOf(scanner.nextLine());
						scanner.nextLine();
						System.out.print("Enter data graduation_rank: ");
						String graduation_rank = String.valueOf(scanner.nextLine());
						System.out.print("Enter data graduation_date: ");
						String graduation_date = String.valueOf(scanner.nextLine());

						System.out.print("Enter number bangcap: ");
						int number = scanner.nextInt();
						scanner.nextLine();
						list = managentEmp.foreachCers(number);
						Employee fresher = new Fresher(id, fullname, birthday, phone, email, list, 1, graduation_date,
								graduation_rank, education);
						managentEmp.addEmployee(fresher);
						System.out.println(fresher.toString());
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
					}

					break;
				}
				case "c": {
					try {
						System.out.println("You will add employee Intern: ");
						List<Certificate> list = new ArrayList<>();
						System.out.println();
						System.out.print("Enter data id: ");
						String id = String.valueOf(scanner.nextLine());
						System.out.print("Enter data fullname: ");
						String fullname = String.valueOf(scanner.nextLine());
						while (!Validator.isFullname(fullname)) {
							System.out.println("Enter fullname again");
							fullname = scanner.nextLine();
						}
						System.out.print("Enter data birthday: ");
						String birthday = scanner.nextLine();
						while (!Validator.isBirthDay(birthday)) {
							System.out.println("Enter date again");
							birthday = scanner.nextLine();
						}
						System.out.print("Enter data phone: ");
						String phone = String.valueOf(scanner.nextLine());
						while (!Validator.isPhone(phone)) {
							System.out.println("Enter phone again");
							phone = scanner.nextLine();
						}
						System.out.print("Enter data email: ");
						String email = String.valueOf(scanner.nextLine());
						while (!Validator.isEmail(email)) {
							System.out.println("Enter email again");
							email = scanner.nextLine();
						}
						System.out.print("Enter majors : ");
						String majors = scanner.nextLine();
						System.out.print("Enter semester: ");
						int semester = scanner.nextInt();
						scanner.nextLine();
						System.out.print("Enter university: ");
						String university = scanner.nextLine();
						System.out.print("Enter number bangcap: ");
						int number = scanner.nextInt();
						scanner.nextLine();
						list = managentEmp.foreachCers(number);
						Employee intern = new Intern(id, fullname, birthday, phone, email, list, 2, majors, semester,
								university);
						managentEmp.addEmployee(intern);
						System.out.println(intern.toString());
					} catch (Exception e2) {
						// TODO: handle exception
						System.out.println(e2);
					}
					break;
				}
				default:
					System.out.println("Try it !!!");
					break;
				}
				break;
			}
			case "4": {

				try {
					System.out.print("Enter id to delete: ");
					String id = String.valueOf(scanner.nextLine());
					managentEmp.deleteById(id);
				} catch (Exception ex) {
					System.out.println(ex);
				}
				break;
			}
			case "3": {

				try {
					System.out.print("Enter id to search: ");
					String id = String.valueOf(scanner.nextLine());
					Employee emp = null;
					emp = managentEmp.findById(id);
					System.out.println(emp.toString());
				} catch (Exception ex) {
					System.out.println(ex);
				}
				break;
			}
			case "5": {
				try {
					System.out.println("You will update employee   ");
					System.out.println();
					System.out.print("Enter data id: ");
					String id = String.valueOf(scanner.nextLine());
					managentEmp.UpdateEmployee(id);
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e);
				}

				break;
			}
			case "2": {
				System.out.println("Enter a: to get Exp");
				System.out.println("Enter b: to get Fresher");
				System.out.println("Enter c: to get Inter");
				String type = scanner.nextLine();
				switch (type) {
				case "a": {
					try {
						System.out.println("get employee Experience: ");
						managentEmp.findByType(0).forEach(e->System.out.println(e));
					} catch (Exception ex) {
						// TODO: handle exception
						System.out.println(ex);
					}

					break;

				}
				case "b": {
					try {
						System.out.println("get employee Fresher: ");
						managentEmp.findByType(1).forEach(e->System.out.println(e));
					} catch (Exception ex) {
						// TODO: handle exception
						System.out.println(ex);
					}
					break;
				}
				case "c": {
					try {
						System.out.println("get employee Intern: ");
						managentEmp.findByType(2).forEach(e->System.out.println(e));
					} catch (Exception ex) {
						// TODO: handle exception
						System.out.println(ex);
					}

					break;
				}
				default:
					System.out.println("Try it !!!");
					break;
				}

				break;

			}
			case "7": {
				return;
			}
			default:
				System.out.println("Try it !!!");
				continue;
			}

		}
	}
}
